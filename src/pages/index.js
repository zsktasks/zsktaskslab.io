import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

import Grid from "../components/grid"

const IndexPage = () => (
  <Layout>
    <h1>Zadania</h1>
    <SEO title="Zadania" />
    <Grid />
  </Layout>
)

export default IndexPage
